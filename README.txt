15-11-2012, Copyright by Cipix Internet. E-mail: info@cipix.nl.

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Known issues
 * Installation

INTRODUCTION
------------

Current Maintainer: Bas van Meurs <bas@cipix.nl>

Drupal automatically hides menu items for paths that are not accessible
by the current visitor. This is often very useful, but occasionally it
is preferable to show the menu item, and show an 'access denied' page
after the user clicks on it.

Unfortunately, there is no such functionality in the Drupal core. This
module aims to fill this gap. It adds a new form element titled
'Ignore access' to the menu link edit screen. To use it, one or more
of the roles should be selected. If a visitor has one of the selected
roles the menu item will always be rendered, even if the current user
has no access to it!

If a visitor clicks on such a link but has no access to it, the standard
Drupal 'access denied' 403 page is shown.

KNOWN ISSUES
------------

None.

INSTALLATION
------------

To install, simply enable the module.
